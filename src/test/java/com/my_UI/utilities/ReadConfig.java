package com.my_UI.utilities;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class ReadConfig {

	Properties properties;
	
	String path = "D:\\UI_Automation_CS_Selenium_JAVA\\UI_Automation_V1\\Configuration\\config.properties";
	
	//create a constructor
	public ReadConfig() {
		try {
			properties = new Properties();	
			FileInputStream fis = new FileInputStream(path);
			properties.load(fis);
		} catch (FileNotFoundException e) {
			//exception for path val is null/empty
			e.printStackTrace();
		} catch (IOException e) {
			//exception for load
			e.printStackTrace();
		}
	}
	
	//now create diff. methods for diff. objs in properties file
	public String getBaseUrl() {
		String value = properties.getProperty("baseUrl");
		if(value != null) 
			return value;
		else
			throw new RuntimeException("baseURL not specified in config file");
	}
	
	public String getBrowser() {
		String value = properties.getProperty("browser");
		if(value != null) { 
			return value;
		}
		else {
			throw new RuntimeException("browser not specified in config file");
		}
	}
}
