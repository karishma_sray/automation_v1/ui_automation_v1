/*Navigate to the Redbus website.
Locate the calendar element.
Iterate through each month in the calendar.
Identify Saturdays and Sundays in each month.
Collect the dates falling on Saturdays and Sundays.*/


package com.my_UI.pageobject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Red_Bus_Cal {
 WebDriver local_Driver; //1.create object of webdriver
 
 //2. create constructor to invoke the for init method of pagefactory
 
 public Red_Bus_Cal(WebDriver remote_Driver) {
	 local_Driver = remote_Driver ;
	 PageFactory.initElements(remote_Driver, this);
 }
 
 //3.Identify webElements
 
 @FindBy(xpath = "//*[class='Date']")
 WebElement date; 
 
 @FindBy(id = "Layer_1") 
 WebElement next_Button;
 
 @FindBy(className ="holiday_count")
 WebElement holiday_Count;
 
 
 //4. create methods to perform actions
 
 public void clickOnDate() {
	 date.click();
 }
		 
		 
}
