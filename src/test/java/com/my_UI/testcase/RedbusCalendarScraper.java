package com.my_UI.testcase;


	import org.openqa.selenium.By;
	import org.openqa.selenium.WebDriver;
	import org.openqa.selenium.WebElement;
	import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.util.List;

	public class RedbusCalendarScraper {
	    public static void main(String[] args) throws InterruptedException {
	        // Set the path to chromedriver.exe
	        System.setProperty("webdriver.chrome.driver", "D:\\UI_Automation_CS_Selenium_JAVA\\UI_Automation_V1\\Drivers\\chromedriver_V123.exe");
	        
	        // Initialize Chrome driver
	       
	        ChromeOptions options = new ChromeOptions();
	        options.addArguments("--remote-allow-origins=*");
	        WebDriver driver = new ChromeDriver(options);	
	        
	        //maximize window
	        driver.manage().window().maximize();
	        
	        Thread.sleep(3000);
	        // Navigate to Redbus website
	        driver.get("https://www.redbus.in/");
	        Thread.sleep(3000);
	        
	        // Click on the calendar icon to open the calendar
	        WebElement calendarIcon = driver.findElement(By.cssSelector("your_calendar_icon_css_selector"));
	        calendarIcon.click();
	        Thread.sleep(5000);
	        // Iterate through each month
	        for (int month = 1; month <= 12; month++) {
	            // Find the calendar month element
	            WebElement calendarMonth = driver.findElement(By.cssSelector("your_calendar_month_css_selector"));
	            
	            // Find all the dates in the current month
	            List<WebElement> dates = calendarMonth.findElements(By.cssSelector("your_date_css_selector"));
	            
	            // Iterate through each date
	            for (WebElement date : dates) {
	                // Check if the date falls on a Saturday or Sunday
	                String day = date.getAttribute("your_attribute_containing_day_name");
	                if (day.equals("Saturday") || day.equals("Sunday")) {
	                    // Print or store the date falling on Saturday or Sunday
	                    System.out.println("Date: " + date.getText());
	                }
	            }
	            
	            // Click on the next month button to move to the next month
	            WebElement nextMonthButton = driver.findElement(By.cssSelector("your_next_month_button_css_selector"));
	            nextMonthButton.click();
	        }
	        
	        // Close the browser
	        driver.quit();
	    }
	}


